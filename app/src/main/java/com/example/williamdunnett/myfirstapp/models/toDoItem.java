package com.example.williamdunnett.myfirstapp.models;

/**
 * Created by williamdunnett on 27/11/2016.
 */

public class toDoItem {

    private int id;
    private String title;
    private String contents;

    public toDoItem()
    {
    }

    public toDoItem(int mId, String mTitle, String mContents)
    {
        this.id = mId;
        this.title = mTitle;
        this.contents = mContents;
    }
    public void setId(int mId) {
        this.id = mId;
    }

    public void setTitle(String mTitle) {
        this.title = mTitle;
    }

    public void setContents(String mContents) {
        this.contents = mContents;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContents() {
        return contents;
    }
}
