package com.example.williamdunnett.myfirstapp.SQLHelper;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by williamdunnett on 27/11/2016.
 */

public class DBHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME;
    private static final String TABLE_ITEM;

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    DATABASE_NAME = 'toDoList';

    // Contacts table name
    TABLE_ITEM = "item";

    // Shops Table Columns names
    private static final String KEY_ID = “id”;
    private static final String KEY_TITLE = “title”;
    private static final String KEY_CONTENTS = “contents”;

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_ITEM = "CREATE TABLE " + TABLE_ITEM + "("
        + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_TITLE + " TEXT,”
        + KEY_SH_ADDR + ” TEXT” + “)”;
        db.execSQL(CREATE_CONTACTS_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// Drop older table if existed
        db.execSQL(“DROP TABLE IF EXISTS ” + TABLE_SHOPS);
// Creating tables again
        onCreate(db);
    }
}

}
