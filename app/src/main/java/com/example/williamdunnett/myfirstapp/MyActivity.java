package com.example.williamdunnett.myfirstapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.SharedPreferences.Editor;


public class MyActivity extends AppCompatActivity {
    public final static String EXTRA_MESSAGE = "com.example.williamdunnett.myfirstapp.MESSAGE";
    ArrayList<String> testNote  = new  ArrayList<String>();
    public static final String prefs = "MyPrefsFile";
    private ListView lv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

       // saveNotes();
        retrieveNotes();
        updateListView();

    }

    @Override
    public void onPause() {
        super.onPause();
        // Do your stuff, e.g. save your application state
        saveNotes();
    }


    /** called when the user presses the Send button */
    public void  sendMessage(View view) {
        EditText editText = (EditText) findViewById(R.id.edit_message);
        if (editText.getText().toString().trim().isEmpty()){
            Toast.makeText(MyActivity.this, "You cannot save a blank message",
                    Toast.LENGTH_SHORT).show();
        }
        else {
            testNote.add(editText.getText().toString()); //To add message to array.
            Log.d("test", "new message added to the array is: " + editText.getText());
            Log.d("test", "current contents of array: " + testNote.toString());
            updateListView();
            editText.setText("");
        }
    }


    public void updateListView(){

        //instantiate custom adapter
        MyCustomAdapter adapter = new MyCustomAdapter(testNote, this);

        //handle listview and assign adapter
        ListView lView = (ListView)findViewById(R.id.lv_items);
        lView.setAdapter(adapter);


    }

    public void saveNotes() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putInt("list_size", testNote.size());
        for (int i=0; i<testNote.size(); i++)
            edit.putString("note_" + i, testNote.get(i));
            edit.apply();

        /*      Editor edit = prefs.edit(); 
        edit.putInt("list_size", testNote.size()); 
        for(int i=0;i<testNote.size(); i++) 
        edit.putString("note_" + i, testNote.get(i)); 
        edit.apply();  
        */

    }

    public void retrieveNotes() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int size = preferences.getInt("list_size", 0);
        ArrayList<String> retrievedTestNote = new ArrayList<String>();

        testNote.clear();

        for (int i=0; i<size; i++)
            testNote.add(preferences.getString("Note_" + i, ""));

  //      testNote.clear();
//        testNote.addAll(retrievedTestNote);


        //  for (int k=0; k<testNote.size(); k++) 
        //     Log.i("contents of array ", testNote.get(k));   
        //retrieving     int size = prefs.getInt("list_size", 0); 

        /*
        ArrayList<String> retrievedTestNote = new ArrayList<String>();
        for(int i=0; i<size; i++)
            retrievedTestNote.add(prefs.getString("Note_" + i, null));
            */


        //testNote.clear();
        //testNote = (ArrayList<String>) retrievedTestNote.clone();

    }


}
