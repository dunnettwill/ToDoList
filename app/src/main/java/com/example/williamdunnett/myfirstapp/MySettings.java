package com.example.williamdunnett.myfirstapp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by williamdunnett on 08/07/16.
 */
public class MySettings {
    private static List<String> data;

    public static void load() {
        data = new ArrayList<String>();
        // use SharedPreferences to retrieve all your data

    }

    public static void save() {
        // save all contents from data
    }

    public static List<String> getData() {
        return data;
    }

    public static void setData(List<String> data) {
        MySettings.data = data;
    }
}
